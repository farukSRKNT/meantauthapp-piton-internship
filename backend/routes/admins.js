const express = require('express');
const router = express.Router();
const passport = require("passport");
const jwt = require("jsonwebtoken");
const config = require("../config/database");
const Admin = require("../models/admin");


// Register
router.post('/register', (req, res, next) => {
  let newAdmin = new Admin({
    name: req.body.name,
    email: req.body.email,
    username: req.body.username,
    password: req.body.password,
  });

  Admin.addAdmin(newAdmin, (err, admin) =>{
    if(err){
      res.json({success: false, msg:"Failed to register admin"});
    }else {
      res.json({success: true, msg:"admin registered"});
    }
  });
});

// Authenticate
router.post('/authenticate', (req, res, next) => {
 const username = req.body.username;
 const password = req.body.password;

 Admin.getAdminByUsername(username, (err, admin) => {
   if(err) throw err;
   if(!admin) {
     return res.json({success: false, msg: 'Admin not found'});
   }


   Admin.comparePassword(password , admin.password, (err, isMatch) => {
     if(err) throw err;
     if(isMatch) {
       const token = jwt.sign({data: admin}, config.secret, {
        expiresIn: 604800
       });

       res.json({
         success: true,
         token: 'JWT ' + token,
         admin : {
           id: admin._id,
           name: admin.name,
           username: admin.username,
           email: admin.email
         }
       });
     } else {
       return res.json({success: false, msg: 'Wrong password'});
     }
   });
 });
});

//Delete Admin
router.delete('/delete/:username', (req, res, next) => {
  const uname = req.params.username;
  Admin.findOneAndRemove({username: uname}, (err) => {
    if(err) throw err;
    else
      return res.json({success: true, msg: 'Admin is deleted successfully'});
  });
});

//List Admins
router.get('/list', (req, res, next) => {
  Admin.find((err, results) => {
    return res.json({admins: results});
  });
});


// Profile
router.get('/profile', passport.authenticate('jwt', {session:false}), (req, res, next) => {
 res.json({admin: req.admin});
});


// Validate
router.get('/validate', (req, res, next) => {
 res.send('VALIDATE');
});


module.exports = router;
