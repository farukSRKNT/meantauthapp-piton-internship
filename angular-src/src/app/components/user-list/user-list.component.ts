import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { ValidateService } from '../../services/validate.service';


@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  name: String;
  username: String;
  email: String;
  password: String;

   names=[];
   usernames=[];
   emails=[];

   constructor(private authService: AuthService,
     private router: Router,
     private flashMessage:FlashMessagesService,
     private validateService: ValidateService
   ) { }

   getList(){
     this.authService.getList().subscribe(data => {
       for(let i in data.users){

         this.names.push(data.users[i].name);
         this.usernames.push(data.users[i].username);
         this.emails.push(data.users[i].email);

     }
    });
  }
   ngOnInit() {
     this.getList();
   }

   registerUser(){
     const user = {
       name: this.name,
       email: this.email,
       username: this.username,
       password: this.password
     }

     // Required Fields
     if(!this.validateService.validateRegister(user)){
       this.flashMessage.show('Please fill in all fields', {cssClass: 'alert-danger', timeout: 3000});
       return false;
     }

     // Validate Email
     if(!this.validateService.validateEmail(user.email)){
       this.flashMessage.show('Please use a valid email', {cssClass: 'alert-danger', timeout: 3000});
       return false;
     }

     //Register User
     this.authService.registerUser(user).subscribe(data => {
     if(data.success) {
       this.flashMessage.show('You registered a user to system', {cssClass: 'alert-success', timeout: 3000});
       location.reload();
     }else {
       this.flashMessage.show('Something went wrong', {cssClass: 'alert-danger', timeout: 3000});
     }
   });

   }

   deleteUser(i){
     console.log(this.usernames[i]);
    this.authService.deleteUser(this.usernames[i]).subscribe(data => {
      if(data.success) {
        this.flashMessage.show("User is deleted successfully", {cssClass: 'alert-success', timeout:2000});
         location.reload();
       }
      else{
          this.flashMessage.show("User couldn't be deleted!", {cssClass: 'alert-danger', timeout:2000});
         }
    });
   }
 }
