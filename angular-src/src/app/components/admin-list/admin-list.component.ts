import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { ValidateService } from '../../services/validate.service';

@Component({
  selector: 'app-admin-list',
  templateUrl: './admin-list.component.html',
  styleUrls: ['./admin-list.component.css']
})

export class AdminListComponent implements OnInit {

  name: String;
  username: String;
  email: String;
  password: String;

  names=[];
  usernames=[];
  emails=[];

  constructor(private authService: AuthService,
    private router: Router,
    private flashMessage:FlashMessagesService,
    private validateService: ValidateService
  ) { }

  getAdminList(){
    this.authService.getAdminList().subscribe(data => {
      for(let i in data.admins){

        this.names.push(data.admins[i].name);
        this.usernames.push(data.admins[i].username);
        this.emails.push(data.admins[i].email);

    }
  });
}

    ngOnInit() {
      this.getAdminList();
    }

    registerAdmin(){
      const admin = {
        name: this.name,
        email: this.email,
        username: this.username,
        password: this.password
      }

      // Required Fields
      if(!this.validateService.validateRegister(admin)){
        this.flashMessage.show('Please fill in all fields', {cssClass: 'alert-danger', timeout: 3000});
        return false;
      }

      // Validate Email
      if(!this.validateService.validateEmail(admin.email)){
        this.flashMessage.show('Please use a valid email', {cssClass: 'alert-danger', timeout: 3000});
        return false;
      }

      //Register Admin
      this.authService.registerAdmin(admin).subscribe(data => {
      if(data.success) {
        this.flashMessage.show('You have registered an admin to system', {cssClass: 'alert-success', timeout: 3000});
        location.reload();
      }else {
        this.flashMessage.show('Something went wrong', {cssClass: 'alert-danger', timeout: 3000});
        this.router.navigate(['/admin-list']);
      }
    });

}
    deleteAdmin(i){
     this.authService.deleteAdmin(this.usernames[i]).subscribe(data => {
       if(data.success) {
         this.flashMessage.show("Admin is deleted successfully", {cssClass: 'alert-success', timeout:2000});
          location.reload();
        }
       else{
           this.flashMessage.show("Admin couldn't be deleted!", {cssClass: 'alert-danger', timeout:2000});
          }
     });
    }
}
