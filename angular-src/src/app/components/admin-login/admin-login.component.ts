import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {

  username: String;
  password: String;

  constructor(
    private authService: AuthService,
    private router: Router,
    private flashMessage:FlashMessagesService
  ) { }

  ngOnInit() {
  }


  onLoginSubmit() {
    const admin = {
      username: this.username,
      password: this.password
    }

    this.authService.authenticateAdmin(admin).subscribe(data => {
      if(data.success && data.admin) {
        this.authService.storeAdminData(data.token, data.admin);
        this.flashMessage.show('You are now logged in as an admin', {cssClass: 'alert-success', timeout:5000});
         this.router.navigate(['admin-list']);
      }else {
        this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout:5000});
         this.router.navigate(['admin-login']);
      }
    });
    }
}
