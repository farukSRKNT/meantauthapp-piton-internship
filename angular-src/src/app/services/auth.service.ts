import { Injectable } from '@angular/core';
import { Http, Headers} from '@angular/http';
import { map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';

const helper = new JwtHelperService();


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  authToken: any;
  user: any;
  admin: any;

  constructor(private http: Http) { }

  registerUser(user) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:3000/users/register', user, {headers: headers})
      .pipe(map(res => res.json()));
  }

  registerAdmin(admin) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:3000/admins/register', admin, {headers: headers})
      .pipe(map(res => res.json()));
  }


  authenticateUser(user) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:3000/users/authenticate', user, {headers: headers})
      .pipe(map(res => res.json()));
  }

  authenticateAdmin(admin) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:3000/admins/authenticate', admin, {headers: headers})
      .pipe(map(res => res.json()));
  }

  getList() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get('http://localhost:3000/users/list', {headers: headers})
      .pipe(map(res => res.json()));
  }

  getAdminList() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get('http://localhost:3000/admins/list', {headers: headers})
      .pipe(map(res => res.json()));
  }

  deleteUser(username) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.delete('http://localhost:3000/users/delete/' + username, {headers: headers})
      .pipe(map(res => res.json()));
  }

  deleteAdmin(username) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.delete('http://localhost:3000/admins/delete/' + username, {headers: headers})
      .pipe(map(res => res.json()));
  }


  getProfile() {
    let headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.get('http://localhost:3000/users/profile', {headers: headers})
      .pipe(map(res => res.json()));
  }

  getAdminProfile() {
    let headers = new Headers();
    this.loadAdminToken();
    headers.append('Authorization', this.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.get('http://localhost:3000/admins/profile', {headers: headers})
      .pipe(map(res => res.json()));
  }

  storeUserData(token, user) {
    this.authToken = null;
    this.user = null;
    this.admin = null;
    localStorage.clear();
    localStorage.setItem('id_token', token);
    localStorage.setItem('user', JSON.stringify(user));
    this.authToken = token;
    this.user = user;
  }

  loadToken() {
    const token = localStorage.getItem('id_token');
    this.authToken = token;
  }

  loadAdminToken() {
    const token = localStorage.getItem('admin_token');
    this.authToken = token;
  }

  loggedIn(){
      const isExpired = !helper.isTokenExpired(localStorage.getItem('id_token'));
      return isExpired;
    }

  storeAdminData(token, admin) {
    this.authToken = null;
    this.user = null;
    this.admin = null;
    localStorage.clear();
    localStorage.setItem('admin_token', token);
    localStorage.setItem('admin', JSON.stringify(admin));
    this.authToken = token;
    this.admin = admin;
  }

  AdminLoggedIn(){
      const isExpired = !helper.isTokenExpired(localStorage.getItem('admin_token'));
      return isExpired;
    }


  logout() {
    this.authToken = null;
    this.user = null;
    this.admin = null;
    localStorage.clear();
  }
}
